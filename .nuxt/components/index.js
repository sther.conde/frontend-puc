export const CardsAuthLoginCard = () => import('../../components/Cards/Auth/LoginCard.vue' /* webpackChunkName: "components/cards-auth-login-card" */).then(c => wrapFunctional(c.default || c))
export const CardsVideosExplanatoryVideoCard = () => import('../../components/Cards/Videos/ExplanatoryVideoCard.vue' /* webpackChunkName: "components/cards-videos-explanatory-video-card" */).then(c => wrapFunctional(c.default || c))
export const FormAuthLoginForm = () => import('../../components/Form/Auth/LoginForm.vue' /* webpackChunkName: "components/form-auth-login-form" */).then(c => wrapFunctional(c.default || c))
export const FormGlobalFileUploadForms = () => import('../../components/Form/Global/FileUploadForms.vue' /* webpackChunkName: "components/form-global-file-upload-forms" */).then(c => wrapFunctional(c.default || c))
export const CardsUsersSummaryAuthorCard = () => import('../../components/Cards/Users/SummaryAuthorCard.vue' /* webpackChunkName: "components/cards-users-summary-author-card" */).then(c => wrapFunctional(c.default || c))
export const CardsUsersSummaryResponsibleCard = () => import('../../components/Cards/Users/SummaryResponsibleCard.vue' /* webpackChunkName: "components/cards-users-summary-responsible-card" */).then(c => wrapFunctional(c.default || c))
export const FormSignsCreateOrEditSignForm = () => import('../../components/Form/Signs/CreateOrEditSignForm.vue' /* webpackChunkName: "components/form-signs-create-or-edit-sign-form" */).then(c => wrapFunctional(c.default || c))
export const LayoutsHeaderLogoLanguageMyAccountHeader = () => import('../../components/Layouts/Header/LogoLanguageMyAccountHeader.vue' /* webpackChunkName: "components/layouts-header-logo-language-my-account-header" */).then(c => wrapFunctional(c.default || c))
export const BreadcrumbPortal = () => import('../../components/Breadcrumb/Global/BreadcrumbPortal.vue' /* webpackChunkName: "components/breadcrumb-portal" */).then(c => wrapFunctional(c.default || c))
export const MenusLanguageTranlateSystemMenu = () => import('../../components/Menus/Language/LanguageTranlateSystemMenu.vue' /* webpackChunkName: "components/menus-language-tranlate-system-menu" */).then(c => wrapFunctional(c.default || c))
export const AvatarGlobalImageAvatar = () => import('../../components/Avatar/Global/ImageAvatar.vue' /* webpackChunkName: "components/avatar-global-image-avatar" */).then(c => wrapFunctional(c.default || c))
export const LogosGlobalInstitutesLogo = () => import('../../components/Logos/Global/InstitutesLogo.vue' /* webpackChunkName: "components/logos-global-institutes-logo" */).then(c => wrapFunctional(c.default || c))
export const LogosGlobalSignbankLogo = () => import('../../components/Logos/Global/SignbankLogo.vue' /* webpackChunkName: "components/logos-global-signbank-logo" */).then(c => wrapFunctional(c.default || c))
export const LogosGlobalSignbankVerticalLogo = () => import('../../components/Logos/Global/SignbankVerticalLogo.vue' /* webpackChunkName: "components/logos-global-signbank-vertical-logo" */).then(c => wrapFunctional(c.default || c))
export const TabsSignsItemCategoryTabs = () => import('../../components/Tabs/Signs/SignsItemCategoryTabs.vue' /* webpackChunkName: "components/tabs-signs-item-category-tabs" */).then(c => wrapFunctional(c.default || c))
export const TableSignsSignTable = () => import('../../components/Table/Signs/SignTable.vue' /* webpackChunkName: "components/table-signs-sign-table" */).then(c => wrapFunctional(c.default || c))
export const UtilsGlobalDetailsTooltipUtils = () => import('../../components/Utils/Global/DetailsTooltipUtils.vue' /* webpackChunkName: "components/utils-global-details-tooltip-utils" */).then(c => wrapFunctional(c.default || c))
export const UtilsGlobalSearchInputUtils = () => import('../../components/Utils/Global/SearchInputUtils.vue' /* webpackChunkName: "components/utils-global-search-input-utils" */).then(c => wrapFunctional(c.default || c))
export const UtilsVideosPlayerVideosUtils = () => import('../../components/Utils/Videos/PlayerVideosUtils.vue' /* webpackChunkName: "components/utils-videos-player-videos-utils" */).then(c => wrapFunctional(c.default || c))
export const FormSignsCategoriesMovementShapesCreateOrEditMovementShapesForm = () => import('../../components/Form/SignsCategories/MovementShapes/CreateOrEditMovementShapesForm.vue' /* webpackChunkName: "components/form-signs-categories-movement-shapes-create-or-edit-movement-shapes-form" */).then(c => wrapFunctional(c.default || c))
export const FormSignsCategoriesWordClassCreateOrEditWordClassForm = () => import('../../components/Form/SignsCategories/WordClass/CreateOrEditWordClassForm.vue' /* webpackChunkName: "components/form-signs-categories-word-class-create-or-edit-word-class-form" */).then(c => wrapFunctional(c.default || c))
export const LayoutsMenusDashboardMenuListDashboard = () => import('../../components/Layouts/Menus/Dashboard/MenuListDashboard.vue' /* webpackChunkName: "components/layouts-menus-dashboard-menu-list-dashboard" */).then(c => wrapFunctional(c.default || c))
export const AvatarUserAvatar = () => import('../../components/Avatar/User/UserAvatar.vue' /* webpackChunkName: "components/avatar-user-avatar" */).then(c => wrapFunctional(c.default || c))
export const LayoutsMenusDefaultMenuDefault = () => import('../../components/Layouts/Menus/Default/MenuDefault.vue' /* webpackChunkName: "components/layouts-menus-default-menu-default" */).then(c => wrapFunctional(c.default || c))
export const LayoutsMenusGlobalMyAccountMenu = () => import('../../components/Layouts/Menus/Global/MyAccountMenu.vue' /* webpackChunkName: "components/layouts-menus-global-my-account-menu" */).then(c => wrapFunctional(c.default || c))
export const TableSignsCategoriesMovementShapesTable = () => import('../../components/Table/SignsCategories/MovementShapes/MovementShapesTable.vue' /* webpackChunkName: "components/table-signs-categories-movement-shapes-table" */).then(c => wrapFunctional(c.default || c))
export const TableSignsCategoriesWordClassTable = () => import('../../components/Table/SignsCategories/WordClass/WordClassTable.vue' /* webpackChunkName: "components/table-signs-categories-word-class-table" */).then(c => wrapFunctional(c.default || c))

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
