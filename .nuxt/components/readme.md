# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<CardsAuthLoginCard>` | `<cards-auth-login-card>` (components/Cards/Auth/LoginCard.vue)
- `<CardsVideosExplanatoryVideoCard>` | `<cards-videos-explanatory-video-card>` (components/Cards/Videos/ExplanatoryVideoCard.vue)
- `<FormAuthLoginForm>` | `<form-auth-login-form>` (components/Form/Auth/LoginForm.vue)
- `<FormGlobalFileUploadForms>` | `<form-global-file-upload-forms>` (components/Form/Global/FileUploadForms.vue)
- `<CardsUsersSummaryAuthorCard>` | `<cards-users-summary-author-card>` (components/Cards/Users/SummaryAuthorCard.vue)
- `<CardsUsersSummaryResponsibleCard>` | `<cards-users-summary-responsible-card>` (components/Cards/Users/SummaryResponsibleCard.vue)
- `<FormSignsCreateOrEditSignForm>` | `<form-signs-create-or-edit-sign-form>` (components/Form/Signs/CreateOrEditSignForm.vue)
- `<LayoutsHeaderLogoLanguageMyAccountHeader>` | `<layouts-header-logo-language-my-account-header>` (components/Layouts/Header/LogoLanguageMyAccountHeader.vue)
- `<BreadcrumbPortal>` | `<breadcrumb-portal>` (components/Breadcrumb/Global/BreadcrumbPortal.vue)
- `<MenusLanguageTranlateSystemMenu>` | `<menus-language-tranlate-system-menu>` (components/Menus/Language/LanguageTranlateSystemMenu.vue)
- `<AvatarGlobalImageAvatar>` | `<avatar-global-image-avatar>` (components/Avatar/Global/ImageAvatar.vue)
- `<LogosGlobalInstitutesLogo>` | `<logos-global-institutes-logo>` (components/Logos/Global/InstitutesLogo.vue)
- `<LogosGlobalSignbankLogo>` | `<logos-global-signbank-logo>` (components/Logos/Global/SignbankLogo.vue)
- `<LogosGlobalSignbankVerticalLogo>` | `<logos-global-signbank-vertical-logo>` (components/Logos/Global/SignbankVerticalLogo.vue)
- `<TabsSignsItemCategoryTabs>` | `<tabs-signs-item-category-tabs>` (components/Tabs/Signs/SignsItemCategoryTabs.vue)
- `<TableSignsSignTable>` | `<table-signs-sign-table>` (components/Table/Signs/SignTable.vue)
- `<UtilsGlobalDetailsTooltipUtils>` | `<utils-global-details-tooltip-utils>` (components/Utils/Global/DetailsTooltipUtils.vue)
- `<UtilsGlobalSearchInputUtils>` | `<utils-global-search-input-utils>` (components/Utils/Global/SearchInputUtils.vue)
- `<UtilsVideosPlayerVideosUtils>` | `<utils-videos-player-videos-utils>` (components/Utils/Videos/PlayerVideosUtils.vue)
- `<FormSignsCategoriesMovementShapesCreateOrEditMovementShapesForm>` | `<form-signs-categories-movement-shapes-create-or-edit-movement-shapes-form>` (components/Form/SignsCategories/MovementShapes/CreateOrEditMovementShapesForm.vue)
- `<FormSignsCategoriesWordClassCreateOrEditWordClassForm>` | `<form-signs-categories-word-class-create-or-edit-word-class-form>` (components/Form/SignsCategories/WordClass/CreateOrEditWordClassForm.vue)
- `<LayoutsMenusDashboardMenuListDashboard>` | `<layouts-menus-dashboard-menu-list-dashboard>` (components/Layouts/Menus/Dashboard/MenuListDashboard.vue)
- `<AvatarUserAvatar>` | `<avatar-user-avatar>` (components/Avatar/User/UserAvatar.vue)
- `<LayoutsMenusDefaultMenuDefault>` | `<layouts-menus-default-menu-default>` (components/Layouts/Menus/Default/MenuDefault.vue)
- `<LayoutsMenusGlobalMyAccountMenu>` | `<layouts-menus-global-my-account-menu>` (components/Layouts/Menus/Global/MyAccountMenu.vue)
- `<TableSignsCategoriesMovementShapesTable>` | `<table-signs-categories-movement-shapes-table>` (components/Table/SignsCategories/MovementShapes/MovementShapesTable.vue)
- `<TableSignsCategoriesWordClassTable>` | `<table-signs-categories-word-class-table>` (components/Table/SignsCategories/WordClass/WordClassTable.vue)
