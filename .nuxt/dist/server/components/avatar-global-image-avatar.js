exports.ids = [1];
exports.modules = {

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(76);

// CONCATENATED MODULE: ./node_modules/vuetify-loader/lib/loader.js??ref--4!./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--7!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Avatar/Global/ImageAvatar.vue?vue&type=template&id=e797607a&


var ImageAvatarvue_type_template_id_e797607a_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "system_grey_3 mr-2 rounded-sm",
    attrs: {
      "id": "ImageAvatar"
    }
  }, [_vm._ssrNode((_vm.image_props ? "<img" + _vm._ssrAttr("src", _vm.image_props) + " width=\"100%\">" : "<!---->") + " "), !_vm.image_props ? _c(VIcon["a" /* default */], {
    staticClass: "my-1 mx-2",
    attrs: {
      "small": "",
      "color": "system_white"
    }
  }, [_vm._v("\n    mdi-account-circle-outline\n  ")]) : _vm._e()], 2);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/Avatar/Global/ImageAvatar.vue?vue&type=template&id=e797607a&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Avatar/Global/ImageAvatar.vue?vue&type=script&lang=js&
/* harmony default export */ var ImageAvatarvue_type_script_lang_js_ = ({
  props: ["image_props"]
});
// CONCATENATED MODULE: ./components/Avatar/Global/ImageAvatar.vue?vue&type=script&lang=js&
 /* harmony default export */ var Global_ImageAvatarvue_type_script_lang_js_ = (ImageAvatarvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(7);

// CONCATENATED MODULE: ./components/Avatar/Global/ImageAvatar.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  Global_ImageAvatarvue_type_script_lang_js_,
  ImageAvatarvue_type_template_id_e797607a_render,
  staticRenderFns,
  false,
  null,
  null,
  "16207143"
  
)

/* harmony default export */ var ImageAvatar = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=avatar-global-image-avatar.js.map