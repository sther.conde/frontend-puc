exports.ids = [9];
exports.modules = {

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VProgressLinear_VProgressLinear_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(225);
/* harmony import */ var _src_components_VProgressLinear_VProgressLinear_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VProgressLinear_VProgressLinear_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(87);
/* harmony import */ var _directives_intersect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(40);
/* harmony import */ var _mixins_colorable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(10);
/* harmony import */ var _mixins_positionable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(26);
/* harmony import */ var _mixins_proxyable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(88);
/* harmony import */ var _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(0);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3);
 // Components

 // Directives

 // Mixins




 // Utilities



const baseMixins = Object(_util_mixins__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"])(_mixins_colorable__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], Object(_mixins_positionable__WEBPACK_IMPORTED_MODULE_4__[/* factory */ "b"])(['absolute', 'fixed', 'top', 'bottom']), _mixins_proxyable__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"]);
/* @vue/component */

/* harmony default export */ __webpack_exports__["a"] = (baseMixins.extend({
  name: 'v-progress-linear',
  directives: {
    intersect: _directives_intersect__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"]
  },
  props: {
    active: {
      type: Boolean,
      default: true
    },
    backgroundColor: {
      type: String,
      default: null
    },
    backgroundOpacity: {
      type: [Number, String],
      default: null
    },
    bufferValue: {
      type: [Number, String],
      default: 100
    },
    color: {
      type: String,
      default: 'primary'
    },
    height: {
      type: [Number, String],
      default: 4
    },
    indeterminate: Boolean,
    query: Boolean,
    reverse: Boolean,
    rounded: Boolean,
    stream: Boolean,
    striped: Boolean,
    value: {
      type: [Number, String],
      default: 0
    }
  },
  data() {
    return {
      internalLazyValue: this.value || 0,
      isVisible: true
    };
  },
  computed: {
    __cachedBackground() {
      return this.$createElement('div', this.setBackgroundColor(this.backgroundColor || this.color, {
        staticClass: 'v-progress-linear__background',
        style: this.backgroundStyle
      }));
    },
    __cachedBar() {
      return this.$createElement(this.computedTransition, [this.__cachedBarType]);
    },
    __cachedBarType() {
      return this.indeterminate ? this.__cachedIndeterminate : this.__cachedDeterminate;
    },
    __cachedBuffer() {
      return this.$createElement('div', {
        staticClass: 'v-progress-linear__buffer',
        style: this.styles
      });
    },
    __cachedDeterminate() {
      return this.$createElement('div', this.setBackgroundColor(this.color, {
        staticClass: `v-progress-linear__determinate`,
        style: {
          width: Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__[/* convertToUnit */ "h"])(this.normalizedValue, '%')
        }
      }));
    },
    __cachedIndeterminate() {
      return this.$createElement('div', {
        staticClass: 'v-progress-linear__indeterminate',
        class: {
          'v-progress-linear__indeterminate--active': this.active
        }
      }, [this.genProgressBar('long'), this.genProgressBar('short')]);
    },
    __cachedStream() {
      if (!this.stream) return null;
      return this.$createElement('div', this.setTextColor(this.color, {
        staticClass: 'v-progress-linear__stream',
        style: {
          width: Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__[/* convertToUnit */ "h"])(100 - this.normalizedBuffer, '%')
        }
      }));
    },
    backgroundStyle() {
      const backgroundOpacity = this.backgroundOpacity == null ? this.backgroundColor ? 1 : 0.3 : parseFloat(this.backgroundOpacity);
      return {
        opacity: backgroundOpacity,
        [this.isReversed ? 'right' : 'left']: Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__[/* convertToUnit */ "h"])(this.normalizedValue, '%'),
        width: Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__[/* convertToUnit */ "h"])(Math.max(0, this.normalizedBuffer - this.normalizedValue), '%')
      };
    },
    classes() {
      return {
        'v-progress-linear--absolute': this.absolute,
        'v-progress-linear--fixed': this.fixed,
        'v-progress-linear--query': this.query,
        'v-progress-linear--reactive': this.reactive,
        'v-progress-linear--reverse': this.isReversed,
        'v-progress-linear--rounded': this.rounded,
        'v-progress-linear--striped': this.striped,
        'v-progress-linear--visible': this.isVisible,
        ...this.themeClasses
      };
    },
    computedTransition() {
      return this.indeterminate ? _transitions__WEBPACK_IMPORTED_MODULE_1__[/* VFadeTransition */ "c"] : _transitions__WEBPACK_IMPORTED_MODULE_1__[/* VSlideXTransition */ "d"];
    },
    isReversed() {
      return this.$vuetify.rtl !== this.reverse;
    },
    normalizedBuffer() {
      return this.normalize(this.bufferValue);
    },
    normalizedValue() {
      return this.normalize(this.internalLazyValue);
    },
    reactive() {
      return Boolean(this.$listeners.change);
    },
    styles() {
      const styles = {};
      if (!this.active) {
        styles.height = 0;
      }
      if (!this.indeterminate && parseFloat(this.normalizedBuffer) !== 100) {
        styles.width = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__[/* convertToUnit */ "h"])(this.normalizedBuffer, '%');
      }
      return styles;
    }
  },
  methods: {
    genContent() {
      const slot = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__[/* getSlot */ "r"])(this, 'default', {
        value: this.internalLazyValue
      });
      if (!slot) return null;
      return this.$createElement('div', {
        staticClass: 'v-progress-linear__content'
      }, slot);
    },
    genListeners() {
      const listeners = this.$listeners;
      if (this.reactive) {
        listeners.click = this.onClick;
      }
      return listeners;
    },
    genProgressBar(name) {
      return this.$createElement('div', this.setBackgroundColor(this.color, {
        staticClass: 'v-progress-linear__indeterminate',
        class: {
          [name]: true
        }
      }));
    },
    onClick(e) {
      if (!this.reactive) return;
      const {
        width
      } = this.$el.getBoundingClientRect();
      this.internalValue = e.offsetX / width * 100;
    },
    onObserve(entries, observer, isIntersecting) {
      this.isVisible = isIntersecting;
    },
    normalize(value) {
      if (value < 0) return 0;
      if (value > 100) return 100;
      return parseFloat(value);
    }
  },
  render(h) {
    const data = {
      staticClass: 'v-progress-linear',
      attrs: {
        role: 'progressbar',
        'aria-valuemin': 0,
        'aria-valuemax': this.normalizedBuffer,
        'aria-valuenow': this.indeterminate ? undefined : this.normalizedValue
      },
      class: this.classes,
      directives: [{
        name: 'intersect',
        value: this.onObserve
      }],
      style: {
        bottom: this.bottom ? 0 : undefined,
        height: this.active ? Object(_util_helpers__WEBPACK_IMPORTED_MODULE_7__[/* convertToUnit */ "h"])(this.height) : 0,
        top: this.top ? 0 : undefined
      },
      on: this.genListeners()
    };
    return h('div', data, [this.__cachedStream, this.__cachedBackground, this.__cachedBuffer, this.__cachedBar, this.genContent()]);
  }
}));

/***/ }),

/***/ 225:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(226);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(6).default("7082b72e", content, true)

/***/ }),

/***/ 226:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".theme--light.v-progress-linear{color:rgba(0,0,0,.87)}.theme--dark.v-progress-linear{color:#fff}.v-progress-linear{background:transparent;overflow:hidden;position:relative;transition:.2s cubic-bezier(.4,0,.6,1);width:100%}.v-progress-linear__buffer{height:inherit;left:0;position:absolute;top:0;transition:inherit;width:100%}.v-progress-linear--reverse .v-progress-linear__buffer{left:auto;right:0}.v-progress-linear__background{bottom:0;left:0;position:absolute;top:0;transition:inherit}.v-progress-linear--reverse .v-progress-linear__background{left:auto;right:0}.v-progress-linear__content{align-items:center;display:flex;height:100%;justify-content:center;left:0;position:absolute;top:0;width:100%}.v-progress-linear--reverse .v-progress-linear__content{left:auto;right:0}.v-progress-linear__determinate{height:inherit;left:0;position:absolute;transition:inherit}.v-progress-linear--reverse .v-progress-linear__determinate{left:auto;right:0}.v-progress-linear .v-progress-linear__indeterminate .long,.v-progress-linear .v-progress-linear__indeterminate .short{animation-play-state:paused;background-color:inherit;bottom:0;height:inherit;left:0;position:absolute;right:auto;top:0;width:auto;will-change:left,right}.v-progress-linear .v-progress-linear__indeterminate--active .long{animation-duration:2.2s;animation-iteration-count:infinite;animation-name:indeterminate-ltr}.v-progress-linear .v-progress-linear__indeterminate--active .short{animation-duration:2.2s;animation-iteration-count:infinite;animation-name:indeterminate-short-ltr}.v-progress-linear--reverse .v-progress-linear__indeterminate .long,.v-progress-linear--reverse .v-progress-linear__indeterminate .short{left:auto;right:0}.v-progress-linear--reverse .v-progress-linear__indeterminate--active .long{animation-name:indeterminate-rtl}.v-progress-linear--reverse .v-progress-linear__indeterminate--active .short{animation-name:indeterminate-short-rtl}.v-progress-linear__stream{animation:stream-ltr .25s linear infinite;animation-play-state:paused;border-color:currentColor;border-top:4px dotted;bottom:0;left:auto;opacity:.3;pointer-events:none;position:absolute;right:-8px;top:calc(50% - 2px);transition:inherit}.v-progress-linear--reverse .v-progress-linear__stream{animation:stream-rtl .25s linear infinite;left:-8px;right:auto}.v-progress-linear__wrapper{overflow:hidden;position:relative;transition:inherit}.v-progress-linear--absolute,.v-progress-linear--fixed{left:0;z-index:1}.v-progress-linear--absolute{position:absolute}.v-progress-linear--fixed{position:fixed}.v-progress-linear--reactive .v-progress-linear__content{pointer-events:none}.v-progress-linear--rounded{border-radius:4px}.v-progress-linear--striped .v-progress-linear__determinate{background-image:linear-gradient(135deg,hsla(0,0%,100%,.25) 25%,transparent 0,transparent 50%,hsla(0,0%,100%,.25) 0,hsla(0,0%,100%,.25) 75%,transparent 0,transparent);background-repeat:repeat;background-size:40px 40px}.v-progress-linear--query .v-progress-linear__indeterminate--active .long{animation-duration:2s;animation-iteration-count:infinite;animation-name:query-ltr}.v-progress-linear--query .v-progress-linear__indeterminate--active .short{animation-duration:2s;animation-iteration-count:infinite;animation-name:query-short-ltr}.v-progress-linear--query.v-progress-linear--reverse .v-progress-linear__indeterminate--active .long{animation-name:query-rtl}.v-progress-linear--query.v-progress-linear--reverse .v-progress-linear__indeterminate--active .short{animation-name:query-short-rtl}.v-progress-linear--visible .v-progress-linear__indeterminate--active .long,.v-progress-linear--visible .v-progress-linear__indeterminate--active .short,.v-progress-linear--visible .v-progress-linear__stream{animation-play-state:running}@keyframes indeterminate-ltr{0%{left:-90%;right:100%}60%{left:-90%;right:100%}to{left:100%;right:-35%}}@keyframes indeterminate-rtl{0%{left:100%;right:-90%}60%{left:100%;right:-90%}to{left:-35%;right:100%}}@keyframes indeterminate-short-ltr{0%{left:-200%;right:100%}60%{left:107%;right:-8%}to{left:107%;right:-8%}}@keyframes indeterminate-short-rtl{0%{left:100%;right:-200%}60%{left:-8%;right:107%}to{left:-8%;right:107%}}@keyframes query-ltr{0%{left:100%;right:-90%}60%{left:100%;right:-90%}to{left:-35%;right:100%}}@keyframes query-rtl{0%{left:-90%;right:100%}60%{left:-90%;right:100%}to{left:100%;right:-35%}}@keyframes query-short-ltr{0%{left:100%;right:-200%}60%{left:-8%;right:107%}to{left:-8%;right:107%}}@keyframes query-short-rtl{0%{left:-200%;right:100%}60%{left:107%;right:-8%}to{left:107%;right:-8%}}@keyframes stream-ltr{to{transform:translateX(-8px)}}@keyframes stream-rtl{to{transform:translateX(8px)}}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(298);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("dc84ee74", content, true, context)
};

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FileUploadForms_vue_vue_type_style_index_0_id_196e7b4d_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(287);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FileUploadForms_vue_vue_type_style_index_0_id_196e7b4d_prod_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FileUploadForms_vue_vue_type_style_index_0_id_196e7b4d_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FileUploadForms_vue_vue_type_style_index_0_id_196e7b4d_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FileUploadForms_vue_vue_type_style_index_0_id_196e7b4d_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, "#file-upload .box-button-change-file{position:absolute;z-index:2}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(202);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(76);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js
var VImg = __webpack_require__(51);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VProgressLinear/VProgressLinear.js
var VProgressLinear = __webpack_require__(219);

// CONCATENATED MODULE: ./node_modules/vuetify-loader/lib/loader.js??ref--4!./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--7!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Form/Global/FileUploadForms.vue?vue&type=template&id=196e7b4d&





var FileUploadFormsvue_type_template_id_196e7b4d_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "text-center mb-4",
    attrs: {
      "id": "file-upload"
    }
  }, [_vm._ssrNode("<div" + _vm._ssrClass("mb-4 warning_light_2 border-lg d-flex flex-column justify-center align-center cursor-pointer h-100 w-100 rounded-lg", _vm.currentFile !== undefined && _vm.currentFile !== null ? 'pa-0' : 'px-1 py-16') + ">", "</div>", [_vm.currentFile !== undefined && _vm.currentFile !== null || _vm.currentVideoUrl || _vm.currentImageUrl ? [_vm.isVideo ? [_vm._ssrNode("<div class=\"box-button-change-file\">", "</div>", [_c(VBtn["a" /* default */], {
    attrs: {
      "fab": "",
      "depressed": "",
      "small": "",
      "color": "warning"
    },
    on: {
      "click": function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        return _vm.onButtonClick.apply(null, arguments);
      }
    }
  }, [_vm.alertSubmit ? _c(VIcon["a" /* default */], {
    attrs: {
      "small": ""
    }
  }, [_vm._v(" mdi-cancel ")]) : _c(VIcon["a" /* default */], {
    attrs: {
      "small": ""
    }
  }, [_vm._v("mdi-cancel")])], 1), _vm._ssrNode(" " + (_vm.alertSubmit ? "<p class=\"system_white--text family-openSans-semi-bold caption-signbank text-center\">" + _vm._ssrEscape("\n            " + _vm._s(_vm.alertSubmit) + "\n          ") + "</p>" : "<!---->"))], 2), _vm._ssrNode(" "), _vm.currentFile !== undefined || _vm.currentVideoUrl ? [_vm.currentFile !== undefined ? _c('video-player', {
    attrs: {
      "src": _vm.currentFile.url
    }
  }) : _c('video-player', {
    attrs: {
      "src": _vm.currentVideoUrl
    }
  })] : _vm._e()] : [_vm._ssrNode("<div class=\"box-button-change-file\">", "</div>", [_c(VBtn["a" /* default */], {
    attrs: {
      "fab": "",
      "depressed": "",
      "small": "",
      "color": "warning"
    },
    on: {
      "click": function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        return _vm.onButtonClick.apply(null, arguments);
      }
    }
  }, [_vm.alertSubmit ? _c(VIcon["a" /* default */], {
    attrs: {
      "small": ""
    }
  }, [_vm._v(" mdi-cancel ")]) : _c(VIcon["a" /* default */], {
    attrs: {
      "small": ""
    }
  }, [_vm._v("mdi-cancel")])], 1), _vm._ssrNode(" " + (_vm.alertSubmit && _vm.currentFile === undefined ? "<p class=\"system_white--text family-openSans-semi-bold caption-signbank text-center\">" + _vm._ssrEscape("\n            " + _vm._s(_vm.alertSubmit) + "\n          ") + "</p>" : "<!---->"))], 2), _vm._ssrNode(" "), _vm.currentFile !== undefined && _vm.currentFile !== null || _vm.currentImageUrl ? [_vm.isImage ? [_vm.currentFile !== undefined ? _c(VImg["a" /* default */], {
    staticStyle: {
      "max-height": "250px"
    },
    attrs: {
      "height": "100%",
      "src": _vm.currentFile.url
    }
  }) : _c(VImg["a" /* default */], {
    attrs: {
      "height": "100%",
      "src": _vm.currentImageUrl
    }
  })] : _c(VIcon["a" /* default */], {
    attrs: {
      "size": "180px",
      "color": "white"
    }
  }, [_vm._v("\n            " + _vm._s(_vm.iconName) + "\n          ")])] : _vm._e()]] : [_c(VBtn["a" /* default */], {
    attrs: {
      "fab": "",
      "dark": "",
      "depressed": "",
      "color": "warning"
    }
  }, [_c(VIcon["a" /* default */], {
    attrs: {
      "color": "white"
    }
  }, [_vm._v("\n          " + _vm._s(_vm.alertSubmit ? "mdi-cancel" : _vm.iconName) + "\n        ")])], 1), _vm._ssrNode(" " + (_vm.label || _vm.alertSubmit ? "<div class=\"mt-2\">" + (_vm.alertSubmit ? "<p class=\"system_grey_2--text family-openSans-semi-bold caption-signbank text-center\">" + _vm._ssrEscape("\n          " + _vm._s(_vm.alertSubmit) + "\n        ") + "</p>" : "<span class=\"system_grey_2--text text-lowercase family-openSans-semi-bold caption-signbank text-center\"><span>" + _vm._s(_vm.label) + "</span></span>") + "</div>" : "<!---->"))]], 2), _vm._ssrNode(" "), _vm.progress_upload ? _c(VProgressLinear["a" /* default */], {
    attrs: {
      "value": _vm.progress_upload
    }
  }) : _vm._e(), _vm._ssrNode(" <input type=\"file\"" + _vm._ssrAttr("accept", _vm.acceptedTypes ? _vm.acceptedTypes : '') + _vm._ssrAttr("multiple", false) + " class=\"d-none\"> "), _vm.currentFile != undefined ? _c(VBtn["a" /* default */], {
    staticClass: "mb-2",
    attrs: {
      "outlined": "",
      "rounded": "",
      "color": "error",
      "loading": _vm.loaderDelete
    },
    on: {
      "click": function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        return _vm.deleteDocuments.apply(null, arguments);
      }
    }
  }, [_c('span', {
    staticClass: "caption-signbank family-openSans-bold"
  }, [_vm._v("\n      " + _vm._s(_vm.$t("buttons.delete")) + "\n    ")])]) : _vm._e(), _vm._ssrNode(" "), _vm.haveNewFile ? _c(VBtn["a" /* default */], {
    staticClass: "mb-2",
    attrs: {
      "outlined": "",
      "rounded": "",
      "color": "warning",
      "loading": _vm.loader,
      "disabled": ""
    },
    on: {
      "click": function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        return _vm.postDocuments.apply(null, arguments);
      }
    }
  }, [_c('span', {
    staticClass: "caption-signbank family-openSans-bold"
  }, [_vm._v("\n      " + _vm._s(_vm.$t("buttons.submit")) + "\n    ")])]) : _c(VBtn["a" /* default */], {
    staticClass: "mb-2",
    attrs: {
      "outlined": "",
      "rounded": "",
      "color": "warning",
      "loading": _vm.loader,
      "disabled": ""
    },
    on: {
      "click": function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        return _vm.onButtonClick.apply(null, arguments);
      }
    }
  }, [_vm.currentFile === null ? _c('span', {
    staticClass: "caption-signbank family-openSans-bold"
  }, [_vm._v("\n      " + _vm._s(_vm.$t("buttons.select")) + "\n    ")]) : [_vm.currentFile !== undefined || _vm.currentVideoUrl || _vm.currentImageUrl ? _c('span', {
    staticClass: "caption-signbank family-openSans-bold"
  }, [_vm._v("\n        " + _vm._s(_vm.$t("buttons.change")) + "\n      ")]) : _c('span', {
    staticClass: "caption-signbank family-openSans-bold"
  }, [_vm._v("\n        " + _vm._s(_vm.$t("buttons.select")) + "\n      ")])]], 2)], 2);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/Form/Global/FileUploadForms.vue?vue&type=template&id=196e7b4d&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Form/Global/FileUploadForms.vue?vue&type=script&lang=js&
/* harmony default export */ var FileUploadFormsvue_type_script_lang_js_ = ({
  props: ["acceptedTypes", "label", "documents_type_id", "currentVideoUrl", "currentImageUrl", "currentFile", "index"],
  data() {
    return {
      loader: false,
      loaderDelete: false,
      form: {
        documents_type_id: null
      },
      progress_upload: 0,
      response: null,
      files: null,
      alertSubmit: "",
      iconName: "",
      isVideo: false,
      isPDF: false,
      isImage: false,
      haveNewFile: false
    };
  },
  mounted() {
    this.verifyTypeFile();
    if (this.documents_type_id) {
      this.form.documents_type_id = this.documents_type_id;
    }
  },
  watch: {
    document_type(val) {
      this.form.documents_type_id = val.id;
    }
  },
  methods: {
    verifyTypeFile() {
      if (this.acceptedTypes === "application/pdf") {
        this.iconName = "mdi-cancel";
        this.isPDF = true;
      } else if (this.acceptedTypes === "video/*") {
        this.iconName = "mdi-cancel";
        this.isVideo = true;
      } else if (this.acceptedTypes === ".png, .jpg, .jpeg") {
        this.iconName = "mdi-cancel";
        this.isImage = true;
      } else if (this.acceptedTypes === "image/*") {
        this.iconName = "mdi-cancel";
        this.isImage = true;
      } else if (this.acceptedTypes === "*/*") {
        this.iconName = "mdi-cancel";
      }
    },
    onButtonClick() {
      this.isSelecting = true;
      window.addEventListener("focus", () => {
        this.isSelecting = false;
      }, {
        once: true
      });
      this.$refs.uploader.click();
    },
    onFileChanged(e) {
      this.files = e.target.files[0];
      if (this.files) {
        this.alertSubmit = this.$t("global_message_alerts.select_video");
        this.haveNewFile = true;
      } else {
        this.haveNewFile = false;
        this.alertSubmit = "";
      }
    },
    deleteDocuments() {
      this.loaderDelete = true;
      this.$axios.delete("documents/" + this.currentFile.id).then(() => {
        this.$toast.success(this.$t("global_message_alerts.success_delete"));
        if (this.index) {
          this.$emit("successDelete", this.index);
        }
        if (!this.index) {
          this.$emit("successDelete");
        }
      }).catch(() => {
        this.$toast.error(this.$t("global_message_alerts.error_delete"));
      }).finally(() => {
        this.loaderDelete = false;
        this.progress_upload = 0;
      });
    },
    postDocuments() {
      if (this.files) {
        let form = new FormData();
        let returno = false;
        this.loader = true;
        this.$emit("submitStatusLoaderNewFile", this.loader);
        this.alertSubmit = this.$t("global_message_alerts.loading");
        form.append("documents", this.files);
        form.append("platform", this.$config.PLATFORM);
        this.$axios.post("documents", form, {
          onUploadProgress: event => {
            let progress = Math.round(event.loaded * 100 / event.total);
            this.progress_upload = progress;
          },
          params: {
            document_type_id: this.form.documents_type_id
          }
        }).then(res => {
          returno = true;
          this.response = res.data.documents;
          if (this.index) {
            this.$emit("successPost", this.response, this.index);
          }
          if (!this.index) {
            this.$emit("successPost", this.response);
          }
          this.$toast.success(this.$t("global_message_alerts.success_create"));
          this.haveNewFile = false;
        }).catch(e => {
          this.$toast.error(this.$t("global_message_alerts.error_create"));
        }).finally(() => {
          this.loader = false;
          this.alertSubmit = "";
          this.$emit("submitStatusLoaderNewFile", this.loader);
        });
      } else {
        this.$toast.error(this.$t("global_message_alerts.select_file"));
      }
    }
  }
});
// CONCATENATED MODULE: ./components/Form/Global/FileUploadForms.vue?vue&type=script&lang=js&
 /* harmony default export */ var Global_FileUploadFormsvue_type_script_lang_js_ = (FileUploadFormsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(7);

// CONCATENATED MODULE: ./components/Form/Global/FileUploadForms.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(297)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  Global_FileUploadFormsvue_type_script_lang_js_,
  FileUploadFormsvue_type_template_id_196e7b4d_render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "98fc9694"
  
)

/* harmony default export */ var FileUploadForms = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=form-global-file-upload-forms.js.map