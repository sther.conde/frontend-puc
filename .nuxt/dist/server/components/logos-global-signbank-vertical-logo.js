exports.ids = [13];
exports.modules = {

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/logo-signbank-vertical.2c5e3a1.svg";

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js
var VImg = __webpack_require__(51);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VProgressCircular/VProgressCircular.js
var VProgressCircular = __webpack_require__(75);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(211);

// CONCATENATED MODULE: ./node_modules/vuetify-loader/lib/loader.js??ref--4!./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--7!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Logos/Global/SignbankVerticalLogo.vue?vue&type=template&id=cf902606&




var SignbankVerticalLogovue_type_template_id_cf902606_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    attrs: {
      "id": "SignbankLogo"
    }
  }, [_c('nuxt-link', {
    staticClass: "d-flex align-center",
    attrs: {
      "to": "/"
    }
  }, [_c(VImg["a" /* default */], {
    staticClass: "logo-img",
    attrs: {
      "width": "100%",
      "max-width": _vm.max_width_props ? _vm.max_width_props : 130,
      "alt": "Logo",
      "src": __webpack_require__(305)
    },
    scopedSlots: _vm._u([{
      key: "placeholder",
      fn: function () {
        return [_c(VRow["a" /* default */], {
          staticClass: "fill-height ma-0",
          attrs: {
            "align": "center",
            "justify": "center"
          }
        }, [_c(VProgressCircular["a" /* default */], {
          attrs: {
            "indeterminate": "",
            "color": "grey lighten-5"
          }
        })], 1)];
      },
      proxy: true
    }])
  })], 1)], 1);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/Logos/Global/SignbankVerticalLogo.vue?vue&type=template&id=cf902606&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Logos/Global/SignbankVerticalLogo.vue?vue&type=script&lang=js&
/* harmony default export */ var SignbankVerticalLogovue_type_script_lang_js_ = ({
  props: ["max_width_props"]
});
// CONCATENATED MODULE: ./components/Logos/Global/SignbankVerticalLogo.vue?vue&type=script&lang=js&
 /* harmony default export */ var Global_SignbankVerticalLogovue_type_script_lang_js_ = (SignbankVerticalLogovue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(7);

// CONCATENATED MODULE: ./components/Logos/Global/SignbankVerticalLogo.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  Global_SignbankVerticalLogovue_type_script_lang_js_,
  SignbankVerticalLogovue_type_template_id_cf902606_render,
  staticRenderFns,
  false,
  null,
  null,
  "5d11dd04"
  
)

/* harmony default export */ var SignbankVerticalLogo = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=logos-global-signbank-vertical-logo.js.map