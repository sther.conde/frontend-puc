exports.ids = [20];
exports.modules = {

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (async (context, locale) => {
  return await Promise.resolve({
    "template": {
      "dashboard": "Dashboard",
      "create": "Create",
      "edit": "Edit",
      "type_search": "Type here what you are search",
      "info_view": "Displaying {count_1} of {count_2}",
      "id": "ID",
      "order_by": "order by",
      "order_by_options": {
        "last": "Last"
      }
    },
    "buttons": {
      "edit": "Edit",
      "delete": "Delete",
      "back": "Back",
      "create": "Create",
      "view_profile": "View Profile",
      "submit": "Submit",
      "select": "Select",
      "change": "Change"
    },
    "categories": {
      "about": "About",
      "contact": "Contact",
      "signs_search": "Signs Search",
      "home": "Home",
      "privacy_policy": 'Privacy Policy',
      "terms_of_user": 'Terms of User',
      "overview": "Overview",
      "users": "Users",
      "signs": "Signs",
      "signs_categories": "Signs Categories",
      "explanatory_video": "Explanatory Videos",
      "language": "Language",
      "categories_sign": {
        "movement_shape": "Movement shapes",
        "word_class": "Word Class"
      },
      "singular_or_plural": {
        "users": "user | users",
        "signs_categories": "sign category | signs categories",
        "signs": "sign | signs",
        "categories_signs": {
          "movement_shape": "movement shape | movement shapes",
          "word_class": "word class | word classes"
        }
      }
    },
    "global": {
      "translate_other_languages": "Translation into other languages ​​on the platform",
      "register": "Register",
      "image": "Image",
      "video": "Vídeo",
      "translate": "Translate",
      "name": "Name",
      "description": "Description",
      "author": "Author",
      "responsible": "Responsible",
      "approver": "Approver",
      "optional": "optional",
      "select": "Select",
      "annex": "Annex",
      "status": {
        "title": "Status",
        "aproved": "Approved",
        "pending": "Pending",
        "yes": "Yes",
        "no": "No"
      },
      "mensager": {
        "no_itens": "No itens",
        "data_required": "Data required",
        "limit_character": "Limit character",
        "min_character": "Min character",
        "exemple_image": "Example image",
        "data_not_informed": "Data not informed",
        "approver_not_informed": "Approver not informed",
        "not_approved": "Not approved",
        "api_mensager": {
          "error": "Error",
          "success": "Success",
          "info": "Info",
          "warning": "Warning",
          "information_no_correct": "Fill in the information correctly"
        },
        "avaliable_code": {
          "title": "To evaluate the Signbank application code, two features were chosen:",
          "item_1": "(1) Create signals",
          "item_2": "(2) Edit signals",
          "note": "Note: Some of the information would need a relationship with other tables. The creation of related tables and the entire CRUD process associated with it is unfeasible due to the fixed time. Therefore, fixed data and changes to the DB proposed in previous activities were necessary."
        }
      }
    },
    "auth": {
      "email": "Email",
      "password": "Password",
      "login": 'Login',
      "forgot_password": "I forgot my password",
      "register": "Register",
      "exit": "Exit"
    },
    "signs": {
      "lemma": "Lemma",
      "gloss_id": "Gloss id",
      "poster": "Poster",
      "basic_properties": "Basic properties",
      "sign_video": "Sign video",
      "image_riwth_sign_frame": "Image with sign framing",
      "syntax": "Syntax",
      "movement": "Movement",
      "status": {
        "pending": "Pending",
        "aproved": "Approved"
      },
      "categories": {
        "movement_shape": "Movement shape",
        "word_class": "Word class",
        "repeated_movement": "Repeated movement",
        "alternating_movement": "Alternating movement"
      }
    }
  });
});

/***/ })

};;
//# sourceMappingURL=lang-en.js.js.map