exports.ids = [21];
exports.modules = {

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (async (context, locale) => {
  return await Promise.resolve({
    "template": {
      "dashboard": "Painel",
      "create": "Criar",
      "edit": "Editar",
      "type_search": "Digite aqui o que você está procurando",
      "info_view": "Exibindo {count_1} de {count_2}",
      "id": "ID",
      "order_by": "ordenar por",
      "order_by_options": {
        "last": "Último"
      }
    },
    "buttons": {
      "edit": "Editar",
      "delete": "Excluir",
      "back": "Voltar",
      "create": "Criar",
      "view_profile": "Ver Perfil",
      "submit": "Enviar",
      "select": "Selecionar",
      "change": "Alterar"
    },
    "categories": {
      "about": "Sobre",
      "contact": "Contato",
      "signs_search": "Pesquisa de Sinais",
      "home": "Início",
      "privacy_policy": 'Política de Privacidade',
      "terms_of_user": 'Termos de Uso',
      "overview": "Visão Geral",
      "users": "Usuários",
      "signs": "Sinais",
      "signs_categories": "Categorias de Sinais",
      "explanatory_video": "Vídeos Explicativos",
      "language": "Idioma",
      "categories_sign": {
        "movement_shape": "Forma de Movimento",
        "word_class": "Classe de Palavra"
      },
      "singular_or_plural": {
        "users": "usuário | usuários",
        "signs_categories": "categoria de sinal | categorias de sinais",
        "signs": "sinal | sinais",
        "categories_signs": {
          "movement_shape": "forma de movimento | formas de movimento",
          "word_class": "classe de palavra | classes de palavras"
        }
      }
    },
    "global": {
      "translate_other_languages": "Tradução para outros idiomas na plataforma",
      "register": "Registrar",
      "image": "Imagem",
      "video": "Vídeo",
      "translate": "Traduzir",
      "name": "Nome",
      "description": "Descrição",
      "author": "Autor",
      "responsible": "Responsável",
      "approver": "Aprovador",
      "optional": "opcional",
      "select": "Selecionar",
      "annex": "Anexo",
      "status": {
        "title": "Status",
        "aproved": "Aprovado",
        "pending": "Pendente",
        "yes": "Sim",
        "no": "Não"
      },
      "mensager": {
        "no_itens": "Sem itens",
        "data_required": "Dados obrigatórios",
        "limit_character": "Limite de caracteres",
        "min_character": "Caracteres mínimos",
        "exemple_image": "Imagem de exemplo",
        "data_not_informed": "Dados não informados",
        "approver_not_informed": "Aprovador não informado",
        "not_approved": "Não aprovado",
        "api_mensager": {
          "error": "Erro",
          "success": "Sucesso",
          "info": "Informação",
          "warning": "Aviso",
          "information_no_correct": "Preencha as informações corretamente"
        },
        "avaliable_code": {
          "title": "Para avaliar o código do aplicativo Signbank, foram escolhidas duas funcionalidades:",
          "item_1": "(1) Criar sinais",
          "item_2": "(2) Editar sinais",
          "note": "Nota: Algumas informações precisariam de relacionamento com outras tabelas. A criação de tabelas relacionadas e todo o processo CRUD associado a isso é inviável devido ao tempo fixo. Portanto, dados fixos e alterações no DB propostas em atividades anteriores foram necessárias."
        }
      }
    },
    "auth": {
      "email": "Email",
      "password": "Senha",
      "login": 'Entrar',
      "forgot_password": "Esqueci minha senha",
      "register": "Registrar",
      "exit": "Sair"
    },
    "signs": {
      "lemma": "Lema",
      "gloss_id": "ID do Glossário",
      "poster": "Autor",
      "basic_properties": "Propriedades Básicas",
      "sign_video": "Vídeo do Sinal",
      "image_riwth_sign_frame": "Imagem com enquadramento do sinal",
      "syntax": "Sintaxe",
      "movement": "Movimento",
      "status": {
        "pending": "Pendente",
        "aproved": "Aprovado"
      },
      "categories": {
        "movement_shape": "Forma de Movimento",
        "word_class": "Classe de Palavra",
        "repeated_movement": "Movimento Repetido",
        "alternating_movement": "Movimento Alternado"
      }
    }
  });
});

/***/ })

};;
//# sourceMappingURL=lang-pt.js.js.map