import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _8f79ea60 = () => interopDefault(import('../pages/about/index.vue' /* webpackChunkName: "pages/about/index" */))
const _79e635a3 = () => interopDefault(import('../pages/contact/index.vue' /* webpackChunkName: "pages/contact/index" */))
const _ae1efdd2 = () => interopDefault(import('../pages/dashboard/index.vue' /* webpackChunkName: "pages/dashboard/index" */))
const _6f46dcd2 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _81a898a8 = () => interopDefault(import('../pages/login/index.vue' /* webpackChunkName: "pages/login/index" */))
const _72ee1dc5 = () => interopDefault(import('../pages/search-sign/index.vue' /* webpackChunkName: "pages/search-sign/index" */))
const _5b5b20d5 = () => interopDefault(import('../pages/dashboard/category-signs/index.vue' /* webpackChunkName: "pages/dashboard/category-signs/index" */))
const _4ed536de = () => interopDefault(import('../pages/dashboard/signs/index.vue' /* webpackChunkName: "pages/dashboard/signs/index" */))
const _16f67410 = () => interopDefault(import('../pages/dashboard/users/index.vue' /* webpackChunkName: "pages/dashboard/users/index" */))
const _40bc804c = () => interopDefault(import('../pages/dashboard/category-signs/create/index.vue' /* webpackChunkName: "pages/dashboard/category-signs/create/index" */))
const _189aee85 = () => interopDefault(import('../pages/dashboard/category-signs/word-class/index.vue' /* webpackChunkName: "pages/dashboard/category-signs/word-class/index" */))
const _50c83e23 = () => interopDefault(import('../pages/dashboard/signs/create/index.vue' /* webpackChunkName: "pages/dashboard/signs/create/index" */))
const _ab4a26c8 = () => interopDefault(import('../pages/dashboard/category-signs/word-class/create/index.vue' /* webpackChunkName: "pages/dashboard/category-signs/word-class/create/index" */))
const _08c3bc22 = () => interopDefault(import('../pages/dashboard/category-signs/word-class/edit/_slug.vue' /* webpackChunkName: "pages/dashboard/category-signs/word-class/edit/_slug" */))
const _58d67bd2 = () => interopDefault(import('../pages/dashboard/category-signs/edit/_slug.vue' /* webpackChunkName: "pages/dashboard/category-signs/edit/_slug" */))
const _1b95c369 = () => interopDefault(import('../pages/dashboard/signs/edit/_slug.vue' /* webpackChunkName: "pages/dashboard/signs/edit/_slug" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _8f79ea60,
    name: "about___pt___default"
  }, {
    path: "/contact",
    component: _79e635a3,
    name: "contact___pt___default"
  }, {
    path: "/dashboard",
    component: _ae1efdd2,
    name: "dashboard___pt___default"
  }, {
    path: "/en",
    component: _6f46dcd2,
    name: "index___en"
  }, {
    path: "/login",
    component: _81a898a8,
    name: "login___pt___default"
  }, {
    path: "/pt",
    component: _6f46dcd2,
    name: "index___pt"
  }, {
    path: "/search-sign",
    component: _72ee1dc5,
    name: "search-sign___pt___default"
  }, {
    path: "/dashboard/category-signs",
    component: _5b5b20d5,
    name: "dashboard-category-signs___pt___default"
  }, {
    path: "/dashboard/signs",
    component: _4ed536de,
    name: "dashboard-signs___pt___default"
  }, {
    path: "/dashboard/users",
    component: _16f67410,
    name: "dashboard-users___pt___default"
  }, {
    path: "/en/about",
    component: _8f79ea60,
    name: "about___en"
  }, {
    path: "/en/contact",
    component: _79e635a3,
    name: "contact___en"
  }, {
    path: "/en/dashboard",
    component: _ae1efdd2,
    name: "dashboard___en"
  }, {
    path: "/en/login",
    component: _81a898a8,
    name: "login___en"
  }, {
    path: "/en/search-sign",
    component: _72ee1dc5,
    name: "search-sign___en"
  }, {
    path: "/pt/about",
    component: _8f79ea60,
    name: "about___pt"
  }, {
    path: "/pt/contact",
    component: _79e635a3,
    name: "contact___pt"
  }, {
    path: "/pt/dashboard",
    component: _ae1efdd2,
    name: "dashboard___pt"
  }, {
    path: "/pt/login",
    component: _81a898a8,
    name: "login___pt"
  }, {
    path: "/pt/search-sign",
    component: _72ee1dc5,
    name: "search-sign___pt"
  }, {
    path: "/dashboard/category-signs/create",
    component: _40bc804c,
    name: "dashboard-category-signs-create___pt___default"
  }, {
    path: "/dashboard/category-signs/word-class",
    component: _189aee85,
    name: "dashboard-category-signs-word-class___pt___default"
  }, {
    path: "/dashboard/signs/create",
    component: _50c83e23,
    name: "dashboard-signs-create___pt___default"
  }, {
    path: "/en/dashboard/category-signs",
    component: _5b5b20d5,
    name: "dashboard-category-signs___en"
  }, {
    path: "/en/dashboard/signs",
    component: _4ed536de,
    name: "dashboard-signs___en"
  }, {
    path: "/en/dashboard/users",
    component: _16f67410,
    name: "dashboard-users___en"
  }, {
    path: "/pt/dashboard/category-signs",
    component: _5b5b20d5,
    name: "dashboard-category-signs___pt"
  }, {
    path: "/pt/dashboard/signs",
    component: _4ed536de,
    name: "dashboard-signs___pt"
  }, {
    path: "/pt/dashboard/users",
    component: _16f67410,
    name: "dashboard-users___pt"
  }, {
    path: "/dashboard/category-signs/word-class/create",
    component: _ab4a26c8,
    name: "dashboard-category-signs-word-class-create___pt___default"
  }, {
    path: "/en/dashboard/category-signs/create",
    component: _40bc804c,
    name: "dashboard-category-signs-create___en"
  }, {
    path: "/en/dashboard/category-signs/word-class",
    component: _189aee85,
    name: "dashboard-category-signs-word-class___en"
  }, {
    path: "/en/dashboard/signs/create",
    component: _50c83e23,
    name: "dashboard-signs-create___en"
  }, {
    path: "/pt/dashboard/category-signs/create",
    component: _40bc804c,
    name: "dashboard-category-signs-create___pt"
  }, {
    path: "/pt/dashboard/category-signs/word-class",
    component: _189aee85,
    name: "dashboard-category-signs-word-class___pt"
  }, {
    path: "/pt/dashboard/signs/create",
    component: _50c83e23,
    name: "dashboard-signs-create___pt"
  }, {
    path: "/en/dashboard/category-signs/word-class/create",
    component: _ab4a26c8,
    name: "dashboard-category-signs-word-class-create___en"
  }, {
    path: "/pt/dashboard/category-signs/word-class/create",
    component: _ab4a26c8,
    name: "dashboard-category-signs-word-class-create___pt"
  }, {
    path: "/",
    component: _6f46dcd2,
    name: "index___pt___default"
  }, {
    path: "/en/dashboard/category-signs/word-class/edit/:slug?",
    component: _08c3bc22,
    name: "dashboard-category-signs-word-class-edit-slug___en"
  }, {
    path: "/pt/dashboard/category-signs/word-class/edit/:slug?",
    component: _08c3bc22,
    name: "dashboard-category-signs-word-class-edit-slug___pt"
  }, {
    path: "/dashboard/category-signs/word-class/edit/:slug?",
    component: _08c3bc22,
    name: "dashboard-category-signs-word-class-edit-slug___pt___default"
  }, {
    path: "/en/dashboard/category-signs/edit/:slug",
    component: _58d67bd2,
    name: "dashboard-category-signs-edit-slug___en"
  }, {
    path: "/en/dashboard/signs/edit/:slug?",
    component: _1b95c369,
    name: "dashboard-signs-edit-slug___en"
  }, {
    path: "/pt/dashboard/category-signs/edit/:slug",
    component: _58d67bd2,
    name: "dashboard-category-signs-edit-slug___pt"
  }, {
    path: "/pt/dashboard/signs/edit/:slug?",
    component: _1b95c369,
    name: "dashboard-signs-edit-slug___pt"
  }, {
    path: "/dashboard/category-signs/edit/:slug",
    component: _58d67bd2,
    name: "dashboard-category-signs-edit-slug___pt___default"
  }, {
    path: "/dashboard/signs/edit/:slug?",
    component: _1b95c369,
    name: "dashboard-signs-edit-slug___pt___default"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
