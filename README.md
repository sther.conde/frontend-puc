# SignBank

Documentation and monitoring tool for signs.

## technologies

- ⚙️ [VUE - 2.6.14](https://br.vuejs.org/v2/guide/installation.html)
- ⚙️ [VUEX - 3.6.2](https://v3.vuex.vuejs.org/)
- ⚙️ [Vuetify - 2.6.1](https://v2.vuetifyjs.com/en/)
- ⚙️ [i18n - 7.2.1](https://i18n.nuxtjs.org/basic-usage)

## pre-requisites

⚠️ Before starting you need to have installed node - v16.20.2 ⚠️

## installation ⤴️⤴️⤴️

```
- git clone https://gitlab.com/sther.conde/puc-front.git
- npm install
- configure your file .env (use env-exemple)
- npm run dev

```

---

## license

For open source projects (🌏), say how it is licensed.

## authors

🌟 [Sther M. Conde](https://gitlab.com/sther.conde)🌟
