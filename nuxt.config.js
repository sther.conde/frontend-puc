import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - signbank-default',
    title: 'signbank-default',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/scss/app.scss",
    "~/assets/scss/typografic-scale.scss",
    "~/assets/scss/font-family.scss",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: "~/plugins/vuetify-themes.js",
      ssr: false,
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

    //Environment Variable
  publicRuntimeConfig: {
    BASE_URL: process.env.BASE_URL,
  },

    
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    //https://i18n.nuxtjs.org/basic-usage
    '@nuxtjs/i18n',
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    //https://www.npmjs.com/package/@nuxtjs/dayjs
    "@nuxtjs/dayjs",
    // https://www.npmjs.com/package/@nuxtjs/toast
    "@nuxtjs/toast",
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_URL,
  },

  // https://www.npmjs.com/package/@nuxtjs/toast
    toast: {
      duration: 5000,
      position: "top-center",
      register: [
        // Register custom toasts
        {
          name: "my-error",
          message: "Oops...Something went wrong",
          options: {
            type: "error",
          },
        },
      ],
  },

  i18n: {
    //https://i18n.nuxtjs.org/strategies/
    strategy: "prefix_and_default", 
    //https://i18n.nuxtjs.org/seo/
    seo: true,
    //https://i18n.nuxtjs.org/lazy-load-translations
    lazy: true,
    langDir: "i18n/",
    defaultLocale: 'pt',
    locales: [
      {
        code: "pt",
        iso: "pt",
        file: "pt.js",
      },

      {
        code: "en",
        iso: "en",
        file: "en.js",
      },
    ]
    
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      dark: false,
    }
  },


  

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
