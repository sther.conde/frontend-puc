export default function ({ $axios, redirect, i18n }, inject) {
    $axios.setHeader("Accept-Language", i18n.locale);
}
  