// https://v2.vuetifyjs.com/en/features/theme/

import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

const vuetify = new Vuetify({
  theme: {
    themes: {
      light: {
        success:'#33CA49',
        error:'#FF5F56',

        warning: '#FF8B00',
        warning_dark: '#B76C00',
        warning_light: '#FFAB47',
        warning_light_2: '#FAF3E7',

        info_dark: '#1D3549',
        info: '#2196F3',
        info_light: '#F6FBFF',

        system_black: '#13260A',
        system_white: '#FFFFFF',

        system_grey_1: '#4D4D4D',
        system_grey_2: '#868E96',
        system_grey_3: '#CED4DA',
        system_grey_4: '#EFF1F4',
 


        primary: '#005a9c',
        primary_dark: '#00376C',
        primary_light: '#c5ceda',

        secondary: '#cbcb98'
      },
    },
  },
})

export default vuetify